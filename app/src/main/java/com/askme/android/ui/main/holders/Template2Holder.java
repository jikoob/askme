package com.askme.android.ui.main.holders;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.askme.android.R;
import com.askme.android.model.Item;
import com.askme.android.model.ListItem;
import com.askme.android.ui.common.AbsRecyclerAdapter;
import com.askme.android.utils.DisplayUtils;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by jyotishman on 05/03/16.
 */


public class Template2Holder extends AbsRecyclerAdapter.ViewHolder{

    private Context context;
    private TextView labelTextView;
    private RecyclerView recyclerView;

    public Template2Holder(Context context,View itemView) {
        super(itemView);
        this.context = context;
        labelTextView = (TextView) itemView.findViewById(R.id.label);
        recyclerView = (RecyclerView) itemView.findViewById(R.id.scroller);
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(new ScrollAdapter(new ArrayList<Item>()));
    }

    public void setView(ListItem item) {
        labelTextView.setText(item.label);
        ((ScrollAdapter)recyclerView.getAdapter()).addAll(item.items);
    }

    public class ScrollAdapter extends RecyclerView.Adapter<ScrollAdapter.ScrollViewHolder>{

        private ArrayList<Item> items;

        public ScrollAdapter(ArrayList<Item> items){
            this.items = items;
        }

        public void addAll(ArrayList<Item> items){
            int start = this.items.size();
            this.items.addAll(items);
            notifyItemRangeInserted(start, items.size());

        }

        @Override
        public ScrollAdapter.ScrollViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(context).inflate(R.layout.scroll_item,parent,false);
            v.setLayoutParams(new ViewGroup.LayoutParams(DisplayUtils.getScreenSize().x/3, ViewGroup.LayoutParams.MATCH_PARENT));

            return new ScrollViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ScrollAdapter.ScrollViewHolder holder, final int position) {
            holder.labelTextView.setText(getItem(position).label);
            Glide.with(context).load(getItem(position).image).placeholder(R.mipmap.ic_launcher).into(holder.imageView);;

                /*.listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        Log.e(ScrollAdapter.class,"getItem(position).image :"+getItem(position).image);
                        if(e==null)
                            return false;
                        e.printStackTrace();
                        Log.e(ScrollAdapter.class, e.getMessage());
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        Log.i(ScrollAdapter.class,"getItem(position).image :"+getItem(position).image);
                        Log.i(ScrollAdapter.class, "resource ready");
                        return false;
                    }
                }).into(holder.imageView);*/
        }

        public Item getItem(int position) {
            return items.get(position);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        public class ScrollViewHolder extends RecyclerView.ViewHolder{

            private ImageView imageView;
            private TextView labelTextView;

            public ScrollViewHolder(View itemView) {
                super(itemView);
                imageView = (ImageView) itemView.findViewById(R.id.imageView);
                labelTextView = (TextView) itemView.findViewById(R.id.label);
            }
        }
    }
}

