package com.askme.android.model;

import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by jyotishman on 03/03/16.
 */
public class ListItem {

    public interface Templates {
        String TEMPLATE_1 = "product-template-1";
        String TEMPLATE_2 = "product-template-2";
        String TEMPLATE_3 = "product-template-3";
    }

    public String label;

    public String image;

    public String template;

    public ArrayList<Item> items;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
