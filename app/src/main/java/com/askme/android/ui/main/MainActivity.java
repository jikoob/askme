package com.askme.android.ui.main;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RelativeLayout;

import com.askme.android.R;
import com.askme.android.ui.common.BaseActivity;


public class MainActivity extends BaseActivity {

    RelativeLayout mFragmentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mFragmentLayout = (RelativeLayout)findViewById(R.id.fragment_container);

        Fragment fragment = Fragment.instantiate(MainActivity.this,MainFragment.class.getName());

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();


    }


}
