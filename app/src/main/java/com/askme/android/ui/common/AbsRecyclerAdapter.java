package com.askme.android.ui.common;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.askme.android.model.ListItem;

import java.util.ArrayList;

/**
 * Created by jyotishman on 03/03/16.
 */
public abstract class AbsRecyclerAdapter<T> extends RecyclerView.Adapter<AbsRecyclerAdapter.ViewHolder> {

    private ArrayList<T> items = new ArrayList<>();

    public void add(T item){
        items.add(item);
        notifyItemInserted(items.size()-1);
    }

    public void addAll(ArrayList<T> items){
        int startPos = this.items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(startPos, items.size());
    }

    public T getItem(int position) {
        return items.get(position);
    }



    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
