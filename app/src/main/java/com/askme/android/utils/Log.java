package com.askme.android.utils;

import com.askme.android.BuildConfig;

/**
 * Created by jyotishman on 03/03/16.
 */
public class Log {

    public static void i (Class klass, String message){
        if (BuildConfig.DEBUG)
            android.util.Log.i(klass.getName(),message);
    }

    public static void e (Class klass, String message){
        if (BuildConfig.DEBUG)
            android.util.Log.e(klass.getName(),message);
    }

    public static void d (Class klass, String message){
        if (BuildConfig.DEBUG)
            android.util.Log.d(klass.getName(),message);
    }

    public static void v (Class klass, String message){
        if (BuildConfig.DEBUG)
            android.util.Log.v(klass.getName(),message);
    }
}
