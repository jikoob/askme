package com.askme.android.ui.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.askme.android.R;
import com.askme.android.model.ListItem;
import com.askme.android.ui.common.AbsRecyclerAdapter;
import com.askme.android.ui.main.holders.Template1Holder;
import com.askme.android.ui.main.holders.Template2Holder;
import com.askme.android.ui.main.holders.Template3Holder;
import com.askme.android.utils.DisplayUtils;


/**
 * Created by jyotishman on 03/03/16.
 */
public class MainRecyclerAdapter extends AbsRecyclerAdapter<ListItem> {

    private Context context;

    public MainRecyclerAdapter(Context context) {
        this.context = context;
    }

    interface TemplateType{
        int ONE = 1;
        int TWO = 2;
        int THREE = 3;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        switch (viewType){
            case TemplateType.ONE:
                v = LayoutInflater.from(context).inflate(R.layout.item_template_1,null,false);
                v.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, DisplayUtils.getScreenSize().y/4));
                return new Template1Holder(context,v);
            case TemplateType.TWO:
                v = LayoutInflater.from(context).inflate(R.layout.item_template_2,null,false);
                v.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,  DisplayUtils.getScreenSize().y/4));
                return new Template2Holder(context,v);
            case TemplateType.THREE:
                v = LayoutInflater.from(context).inflate(R.layout.item_template_3,null,false);
                v.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,  DisplayUtils.getScreenSize().y/4));
                return new Template3Holder(context,v);
        }

        throw new IllegalStateException("illegal viewType :"+viewType);
    }

    @Override
    public void onBindViewHolder(AbsRecyclerAdapter.ViewHolder holder, int position) {

        int viewType = getItemViewType(position);

        switch (viewType){
            case TemplateType.ONE:
                ((Template1Holder) holder).setImage(getItem(position).items.get(0).image);
                break;
            case TemplateType.TWO:
                ((Template2Holder) holder).setView(getItem(position));
                break;
            case TemplateType.THREE:
                ((Template3Holder) holder).setView(getItem(position));
                break;
        }


    }

    @Override
    public int getItemViewType(int position) {
        ListItem listItem = getItem(position);
        switch (listItem.template){
            case ListItem.Templates.TEMPLATE_1:
                return TemplateType.ONE;
            case ListItem.Templates.TEMPLATE_2:
                return TemplateType.TWO;
            case ListItem.Templates.TEMPLATE_3:
                return TemplateType.THREE;
        }
        return -1;
    }



}
