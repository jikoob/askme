package com.askme.android.utils;

import com.askme.android.App;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by jyotishman on 03/03/16.
 */
public class JSONUtils {
    public static String loadJSONFromAsset(String fileName) throws IOException{
        String json = null;
        try {
            InputStream is = App.getAppContext().getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
           throw ex;
        }
        return json;
    }

    public static InputStream loadJSONStreamFromAsset(String fileName) throws IOException{
        try {
            return App.getAppContext().getAssets().open(fileName);

        } catch (IOException ex) {
            throw ex;
        }
    }
}
