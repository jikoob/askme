package com.askme.android;

import android.app.Application;
import android.content.Context;

/**
 * Created by jyotishman on 03/03/16.
 */
public class App extends Application {

    private static Context appContext;

    public static Context getAppContext(){
        return appContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = App.this;
    }
}
