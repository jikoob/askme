package com.askme.android.utils.deserialiser;

import com.askme.android.model.Item;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

/**
 * Created by jyotishman on 04/03/16.
 */
public class ItemDeserialiser implements JsonDeserializer<Item> {
    @Override
    public Item deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        final JsonObject jsonObject = json.getAsJsonObject();

        String imageUrl = "";
        JsonElement imageUrlObj = jsonObject.get("image");
        if (imageUrlObj != null)
            imageUrl = imageUrlObj.getAsString();
        else
            imageUrl = jsonObject.get("image_url").getAsString();

        Item item = new Item();

        item.image = imageUrl;
        item.label = jsonObject.get("label").getAsString();


        return item;
    }
}
