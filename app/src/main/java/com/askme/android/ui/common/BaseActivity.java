package com.askme.android.ui.common;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.askme.android.R;

/**
 * Created by jyotishman on 05/03/16.
 */
public class BaseActivity extends FragmentActivity {

    private Menu mMenu;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setupActionBar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mMenu = menu;
        getMenuInflater().inflate(R.menu.main, menu);
        RelativeLayout notificationLayout = (RelativeLayout) menu.findItem(R.id.action_cart).getActionView();
        TextView tv = (TextView) notificationLayout.findViewById(R.id.actionbar_notify_text);

        return true;
    }


    private void setupActionBar() {
        ActionBar actionBar = getActionBar();

        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

}
