package com.askme.android.ui.main;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.askme.android.model.ListItem;
import com.askme.android.ui.common.AbsRecyclerAdapter;
import com.askme.android.ui.common.BaseRecyclerFragment;
import com.askme.android.utils.tasks.JSONParserTask;

/**
 * Created by jyotishman on 03/03/16.
 */
public class MainFragment extends BaseRecyclerFragment implements JSONParserTask.IJSONParserCallbacks{

    private JSONParserTask jsonParserTask;

    MainRecyclerAdapter mainRecyclerAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        jsonParserTask = new JSONParserTask(MainFragment.this);
        jsonParserTask.execute(Integer.toString(JSONParserTask.JSONSource.ASSETS),"f_two.json");
        mainRecyclerAdapter = new MainRecyclerAdapter(getActivity());
    }

    @Override
    protected AbsRecyclerAdapter getAdapter() {
        return mainRecyclerAdapter;
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
    }


    @Override
    public void onExecuteParamsError(Throwable throwable) {

    }

    @Override
    public void onListItemFetched(ListItem listItem) {
        mainRecyclerAdapter.add(listItem);
    }
}
