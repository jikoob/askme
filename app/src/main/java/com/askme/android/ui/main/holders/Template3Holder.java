package com.askme.android.ui.main.holders;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.askme.android.R;
import com.askme.android.model.Item;
import com.askme.android.model.ListItem;
import com.askme.android.ui.common.AbsRecyclerAdapter;
import com.askme.android.ui.widgets.BubbleViewPagerIndicator;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by jyotishman on 05/03/16.
 */


public class Template3Holder extends AbsRecyclerAdapter.ViewHolder{

    private BubbleViewPagerIndicator pagerIndicator;
    private ViewPager viewPager;
    private Context context;

    public Template3Holder(Context context,View itemView) {
        super(itemView);
        this.context = context;
        viewPager = (ViewPager) itemView.findViewById(R.id.pager);
        pagerIndicator = (BubbleViewPagerIndicator) itemView.findViewById(R.id.pager_indicator);
    }


    public void setView(final ListItem item) {
        viewPager.setAdapter(new CustomPagerAdapter(item.items));
        pagerIndicator.makeBubbles(R.drawable.bg_bubble_indicator,item.items.size());
        pagerIndicator.setBubbleActive(0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                pagerIndicator.setBubbleActive(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    class CustomPagerAdapter extends PagerAdapter {

        LayoutInflater mLayoutInflater;
        ArrayList<Item> items;

        public CustomPagerAdapter(ArrayList<Item> items) {
            this.items = items;
            mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);


            Glide.with(context).load(items.get(position).image).placeholder(R.drawable.ic_launcher).into(imageView);

            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }
}
