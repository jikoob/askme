package com.askme.android.utils.tasks;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.askme.android.model.Item;
import com.askme.android.model.ListItem;
import com.askme.android.utils.JSONUtils;
import com.askme.android.utils.deserialiser.ItemDeserialiser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by jyotishman on 03/03/16.
 */
public class JSONParserTask extends AsyncTask<String, ListItem, ArrayList<ListItem>> {

    private IJSONParserCallbacks callbacks;

    private Throwable executeThrowable;


    public JSONParserTask(@NonNull IJSONParserCallbacks callbacks){
        this.callbacks = callbacks;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected ArrayList<ListItem> doInBackground(String... params) {

        int jsonSource = -1;
        try {
            jsonSource = Integer.parseInt(params[0]);
        } catch (NumberFormatException nfe) {
            executeThrowable = nfe;
            return null;
        }

        switch (jsonSource) {
            case JSONSource.ASSETS:
                String fileName = params[1];
                if (fileName == null || fileName.trim().length() == 0) {
                    executeThrowable = new IllegalArgumentException("file name param is not valid");
                    return null;
                }
                try {
                    return parseAssetFile(fileName);
                } catch (IOException e) {
                    executeThrowable = new IllegalArgumentException("file name param is not valid");
                }
                break;
            case JSONSource.NETWORK:
                //TODO implement this
                break;
            default:
                executeThrowable = new IllegalArgumentException("params[0] must be a valid JSONSource ");
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(ListItem... values) {
        super.onProgressUpdate(values);
        if (callbacks != null)
            callbacks.onListItemFetched(values[0]);
    }

    @Override
    protected void onPostExecute(ArrayList<ListItem> items) {
        super.onPostExecute(items);
        if (items == null || items.size() == 0) {
            if (executeThrowable != null && callbacks != null) {
                callbacks.onExecuteParamsError(executeThrowable);
            }
            return;
        }

    }

    private ArrayList<ListItem> parseAssetFile(String fileName) throws IOException {

        ListItem listItem;
        ArrayList<ListItem> items = null;

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Item.class, new ItemDeserialiser());
        Gson gson = gsonBuilder.create();

        try {
            JsonReader jsonReader = new JsonReader(new InputStreamReader(JSONUtils.loadJSONStreamFromAsset(fileName)));
            jsonReader.beginArray();
            while (jsonReader.hasNext()) {
                listItem = gson.fromJson(jsonReader, ListItem.class);
                if (listItem != null) {
                    if (items == null)
                        items = new ArrayList<>();
                    items.add(listItem);
                    publishProgress(listItem);
                }
            }
            jsonReader.endArray();
            jsonReader.close();
            return items;
        } catch (IOException e) {
            throw e;
        }
    }


    public interface JSONSource {
        int ASSETS = 0;
        int NETWORK = 1;
    }

    public interface IJSONParserCallbacks {

        void onExecuteParamsError(Throwable throwable);

        void onListItemFetched(ListItem listItem);
    }
}
