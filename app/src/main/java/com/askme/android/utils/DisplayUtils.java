package com.askme.android.utils;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

import com.askme.android.App;

/**
 * Created by jyotishman on 05/03/16.
 */
public class DisplayUtils {

    public static Point getScreenSize(){
        WindowManager wm = (WindowManager) App.getAppContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;

    }
}
