package com.askme.android.ui.main.holders;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.askme.android.R;
import com.askme.android.ui.common.AbsRecyclerAdapter;
import com.bumptech.glide.Glide;

/**
 * Created by jyotishman on 05/03/16.
 */

public class Template1Holder extends AbsRecyclerAdapter.ViewHolder{

    public ImageView imageView;
    private Context context;

    public Template1Holder(Context context,View itemView) {
        super(itemView);
        this.context = context;
        imageView = (ImageView) itemView.findViewById(R.id.image);
    }

    public void setImage(String image) {
        Glide.with(context).load(image).into(imageView);

    }
}
