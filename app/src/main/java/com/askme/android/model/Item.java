package com.askme.android.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jyotishman on 03/03/16.
 */
public class Item {

    public String label;

    public String image;

    @SerializedName("web-url")
    public String webUrl;



}
